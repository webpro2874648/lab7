type Member = {
  id: number,
  name: string,
  tel: string
}

export { type Member }